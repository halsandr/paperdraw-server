import express, { Express, Request, Response } from "express";
import bodyParser from "body-parser";
import { writeFile } from 'fs';
import path from "path";
import dotenv from "dotenv";
import sharp from "sharp";
import crypto from "crypto";

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 3000;
const width = 250;
const height = 128;
const imageLocation = path.join(__dirname, 'public/saved/image.png');

app.use(bodyParser.json({ limit: '1mb' }));
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use('/script', express.static(path.join(__dirname, 'script')));

app.get("/", (req: Request, res: Response) => {
	res.sendFile(path.join(__dirname, "public/index.html"));
});

app.get("/status", (req: Request, res: Response) => {
	res.status(200).send("Server is running");
});

app.post("/save", (req: Request, res: Response) => {
	//console.log(req);
	const dataUrl = req.body.dataUrl;

	if(!dataUrl.startsWith('data:image/png;base64,')) {
		console.error("Invalid PNG data URL");
		return res.status(400).send("Invalid PNG data URL");
	}

	const base64Data = dataUrl.replace(/^data:image\/png;base64,/, '');
	const buffer = Buffer.from(base64Data, 'base64');
	writeFile(imageLocation, buffer, (err) => {
		if (err) {
			console.error(err);
			return res.status(500).send("Failed to save image");
		}

		console.log("Image saved successfully");
		res.send("Image saved successfully");
	});
});

app.get("/image_resized.png", (req: Request, res: Response) => {
	const imagePath = imageLocation;
	const newImage = resizeImage(imagePath, width, height);
	
	// Return the resized image
	newImage.then((data) => {
		res.type('image/png');
		res.send(data);
	}).catch((err) => {
		res.status(500).send("Failed to resize image");
		console.error(err);
	});
});

app.get("/image_resized", (req: Request, res: Response) => {
	const imagePath = imageLocation;
	const newImage = resizeImage(imagePath, width, height);
	
	// Return the resized image as a byte array
	newImage.then((data) => {
		return imageBufferToByteArray(data);
	}).then((imageArray) => {
		res.type('application/octet-stream');
		res.send(Buffer.from(imageArray));
	}).catch((err) => {
		res.status(500).send("Failed to resize image");
		console.error(err);
	});
});

app.get("/image_resized_reversed.png", (req: Request, res: Response) => {
	const imagePath = imageLocation;
	const newImage = resizeImage(imagePath, width, height);
	
	// Return the resized image as a byte array
	newImage.then((data) => {
		return imageBufferToByteArray(data);
	}).then((byteArray) => {
		const imageBuffer = byteArrayToImageBuffer(byteArray, width, height);
		return sharp(imageBuffer, { raw: { width: width, height: height, channels: 4 } }).png().toBuffer();
	}).then((image) => {
		res.type('image/png');
		res.send(image);
	}).catch((err) => {
		console.warn(err);
		res.status(500).send("Failed to reverse image");
	});
});

app.get("/image_hash", (req: Request, res: Response) => {
	const imagePath = imageLocation;
	const newImage = resizeImage(imagePath, width, height);
	
	// Return the resized image as a byte array
	newImage.then((data) => {
		return imageBufferToByteArray(data);
	}).then((byteArray) => {
		const imageHash = getImageHash(byteArray);
		res.send(imageHash);
	}).catch((err) => {
		res.status(500).send("Failed to calculate image hash");
		console.error(err);
	});
});

const resizeImage = async (imagePath: string, width: number, height: number): Promise<Buffer> => {
	const newImage = await sharp(imagePath)
		.resize(width, height, {
			fit: 'contain', 
			background: { r: 0, g: 0, b: 0, alpha: 0 }
		})
		.toBuffer();
	
	return newImage;
}

const imageBufferToByteArray = async (imageBuffer: Buffer): Promise<Uint8Array> => {
	const binaryImageBuffer = await sharp(imageBuffer)
		.threshold(100)
		.raw()
		.toBuffer();

	const { width, height, channels } = await sharp(imageBuffer).metadata();
	if (!width || !height || !channels) {
		throw new Error("Failed to get image metadata");
	}
	
	if(channels !== 4) {
		throw new Error("Invalid image format. Image must be in RGBA format.");
	}
	
	const byteArray = new Uint8Array(Math.ceil(width * height) / 8);

	if(byteArray.length !== Math.floor(binaryImageBuffer.length / (4 * 8))) {
		throw new Error("Invalid image size");
	}
	
	// Convert RGBA into black/white byte array
	for (let i = 3; i < binaryImageBuffer.length; i += 4) {	
		// Check if the pixel is not transparent
		const isBlack = (binaryImageBuffer[i] === 0) ? 0 : 1;

		const byteArrayIndex = Math.floor(i / (4 * 8));

		// Set the bit in the byte array
		// Each byte contains 8 pixels
		// Each pixel is represented by 1 bit
		let currentByte = byteArray[byteArrayIndex];

		// Calculate the bit position in the byte
		const bitPosition = Math.floor(i / 4) % 8;
		const mask = 1 << bitPosition;

		// Set the bit in the byte
		if(isBlack) {
			currentByte = currentByte | mask;
		} else {
			currentByte = currentByte & ~mask;
		}

		// Update the byte array
		byteArray[byteArrayIndex] = currentByte;
	}

	return byteArray;
}

const byteArrayToImageBuffer = (byteArray: Uint8Array, width: number, height: number): Buffer => {
	// Reverse the process for testing
	const binaryImageBuffer = Buffer.alloc(width * height * 4);
	
	for (let i = 0; i < byteArray.length; i++) {
		const byte = byteArray[i];
		for (let j = 0; j < 8; j++) {
			const bit = (byte >> j) & 1;
			const pixelIndex = i * 8 + j;
			const bufferIndex = pixelIndex * 4;

			// Set the pixel color
			binaryImageBuffer[bufferIndex] = 0;
			binaryImageBuffer[bufferIndex + 1] = 0;
			binaryImageBuffer[bufferIndex + 2] = 0;
			binaryImageBuffer[bufferIndex + 3] = bit * 255;
		}
	}
	
	return binaryImageBuffer;
}

const getImageHash = (byteArray: Uint8Array): string => {
	const hash = crypto.createHash('sha256');
	hash.update(byteArray);
	return hash.digest('hex');
}

app.listen(port, () => {
	console.log(`[server]: Server is running at http://localhost:${port}`);
});

